import java.util.*

/*
author: David Moreno Fernandez
date: 14/12/2022
exercise: Eleva’l
 */

fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix el primer numero: ")
    val firstNumber = scanner.nextInt()
    println("Introdueix el segon numero: ")
    val secondNumber = scanner.nextInt()
    println("Aquest es el resultat: ")
    println(elevateNumber(firstNumber, secondNumber))
}

fun elevateNumber(firstNumber: Int, secondNumber: Int): Int{
    var resultat= 1
    for (i in 1..secondNumber){
        resultat *= firstNumber
    }
    return resultat
}