import java.util.*

/*
author: David Moreno Fernandez
date: 14/12/2022
exercise: Escriu una línia
 */

fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix el numero d'espais: ")
    val spaceNumber = scanner.nextInt()
    println("Introdueix el numero de caracters: ")
    val charNumber = scanner.nextInt()
    println("Introdueix el caracter: ")
    val char= scanner.next().single()
    println("Aquest es el resultat: ")
    print(writeALine(spaceNumber, charNumber, char))

}

fun writeALine(spaceNumber: Int, charNumber: Int, char: Char): String{
    var linea= ""
    repeat(spaceNumber){
        linea += " "
    }
    repeat(charNumber){
        linea += char
    }
    return linea
}