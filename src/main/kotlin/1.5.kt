import java.util.*

/*
author: David Moreno Fernandez
date: 14/12/2022
exercise: D’String a MutableList<Char>
 */

fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix l'string: ")
    val string = scanner.nextLine()
    println(stringToMutableList(string))
}
fun stringToMutableList(string: String): MutableList<Char>{
    val list= mutableListOf<Char>()
    for (i in string){
        list.add(i)
    }
    return list
}