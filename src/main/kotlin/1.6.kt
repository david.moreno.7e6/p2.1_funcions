import java.util.*

/*
author: David Moreno Fernandez
date: 14/12/2022
exercise: Divideix un String
 */

fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix l'string: ")
    val string = scanner.nextLine()
    println("introdueix el caracter:")
    val caracter= scanner.next().single()
    println(separateString(string,caracter))
}
fun separateString(string: String, caracter: Char): MutableList<String>{
    val newString= string.split(caracter)
    val list= mutableListOf<String>()
    for (i in 0..newString.lastIndex){
        if (newString[i] != ""){
            list.add(newString[i])
        }

    }
    return list
}