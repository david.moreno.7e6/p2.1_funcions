import java.util.*

/*
author: David Moreno Fernandez
date: 14/12/2022
exercise: Caràcter d’un String
 */

fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix l'string: ")
    val firstString = scanner.nextLine()
    println("Introdueix el numero: ")
    val numero = scanner.nextInt()
    println(positionInString(firstString, numero))
}

fun positionInString(string: String,numero: Int ): String{
    if (numero <= string.lastIndex) {return "L'string es ${string[numero].toString()}"}
    else return "Error, la mida del string es inferior al numero. ${string.lastIndex}<$numero"
}