import java.util.*

/*
author: David Moreno Fernandez
date: 14/12/2022
exercise: Subseqüència
 */

fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix l'string: ")
    val string = scanner.nextLine()
    println("Introdueix el primer numero del domini: ")
    val numeroPrincipi = scanner.nextInt()
    println("Introdueix l'ultim numero del domini:")
    val numeroFinal = scanner.nextInt()
    println(subsecuenceString(string,numeroPrincipi,numeroFinal))
}
fun subsecuenceString(string: String, numeroPrincipi: Int, numeroFinal: Int): String{
    var secuence=""
    if (numeroFinal> string.lastIndex){ return "La subsecuencia $numeroPrincipi-$numeroFinal no existeix"}
    else
        for (i in numeroPrincipi..numeroFinal){
        secuence += string[i]
    }
    return "La subsecuencia $numeroPrincipi-$numeroFinal pertany a $secuence"
}