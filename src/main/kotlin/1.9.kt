import java.util.*

/*
author: David Moreno Fernandez
date: 14/12/2022
exercise: Escriu una creu
 */

fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix el numero de caracters: ")
    val charNumber = scanner.nextInt()
    println("Introdueix el caracter: ")
    val char = scanner.next().single()
    println("Aquest es el resultat: ")
//    for (i in 0 until charNumber){
//        println(crossCreator(charNumber, char)[i])
//    }
    for(i in 0 until charNumber){
        println(crossCreatorAlternative(charNumber, char)[i])
    }
}
//fun crossCreator(charNumber: Int, char: Char): MutableList<MutableList<String>>{
//    val cruz= MutableList(charNumber){MutableList(charNumber){" "}}
//
//    for (i in 0 until charNumber){
//        for (l in 0 until charNumber){
//            if (i == charNumber/2){
//                cruz[i][l]= char.toString()
//            }
//            else if (i != charNumber/2 && l== charNumber/2){
//                cruz[i][l]= char.toString()
//            }
//        }
//    }
//
//    return cruz
//}

fun crossCreatorAlternative(charNumber: Int, char: Char): MutableList<String>{
    val cruz= mutableListOf<String>()
    var filas= ""
    for (i in 0 until charNumber){
        filas= ""
        if (i == charNumber/2){
            repeat(charNumber){
                filas += char
            }
            cruz.add(filas)
        }
        else {
            repeat(charNumber/2){
                filas += " "
            }
            repeat(1){
                filas += char
            }
            repeat(charNumber/2){
                filas += " "
            }
            cruz.add(filas)
        }
    }
    return cruz
}