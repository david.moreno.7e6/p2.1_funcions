import java.util.*

/*
author: David Moreno Fernandez
date: 14/12/2022
exercise: Escriu un marc per un String
 */

fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix la frase: ")
    val frase = scanner.nextLine()
    println("Introdueix el caracter: ")
    val char = scanner.next().single()
    println("Aquest es el resultat: ")
    for (i in 0 until 5){
        println(frameForString(frase, char)[i])
    }
}

fun frameForString( string: String, char: Char): MutableList<String>{
    val frame= mutableListOf<String>()
    var linea= ""

    for (i in 0 until 5){
        linea= ""
        if (i ==2){
            linea= char + " " + string + " "+ char
            frame.add(linea)
        }
        else if (i == 0 || i == 4){
            repeat(string.length + 4){
                linea += char
            }
            frame.add(linea)
        }
        else {
            linea += char

            repeat(string.length + 2){
                linea += " "
            }
            linea += char

            frame.add(linea)
        }
    }
    return frame
}