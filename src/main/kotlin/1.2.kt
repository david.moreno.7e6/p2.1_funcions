import java.util.Scanner

/*
author: David Moreno Fernandez
date: 14/12/2022
exercise: String és igual
 */

fun main(){
    val scanner= Scanner(System.`in`)
    println("Introdueix el primer string: ")
    val firstString= scanner.nextLine()
    println("Introdueix el segon string: ")
    val secondString= scanner.nextLine()
    println("Els strings son iguals ?")
    println(sameString(firstString, secondString))
}

fun sameString (first: String, second: String): Boolean{
    return first == second
}